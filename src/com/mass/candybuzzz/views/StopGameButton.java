package com.mass.candybuzzz.views;

import com.mass.candybuzzz.GameManager;
import com.mass.candybuzzz.GameView;
import com.mass.candybuzzz.R;
import android.graphics.BitmapFactory;

public class StopGameButton extends ButtonView{
	GameView view;

	public StopGameButton(GameView view) {
		super(BitmapFactory.decodeResource(view.getResources(), R.drawable.shutdown));
		this.view = view;
	}

	public void press(GameManager gameManager) {
		gameManager.restartGame();
		if(!gameManager.isLovesFinish()) {
			view.setResetButton(new ResetLevelButton(view));
		}
	}

}
