package com.mass.candybuzzz.views;

import com.mass.candybuzzz.GameManager;
import com.mass.candybuzzz.GameView;
import com.mass.candybuzzz.R;
import android.graphics.BitmapFactory;

public class ResetLevelButton extends ButtonView{
	GameView view;

	public ResetLevelButton(GameView view) {
		super(BitmapFactory.decodeResource(view.getResources(), R.drawable.restart));
		this.view = view;
	}

	public void press(GameManager gameManager) {
		gameManager.restartLevel();
		if(gameManager.isLovesFinish()) {
			view.setResetButton(new StopGameButton(view));
		}
	}

}
