package com.mass.candybuzzz.views;

import com.mass.candybuzzz.GameManager;
import com.mass.candybuzzz.GameView;
import com.mass.candybuzzz.R;
import android.graphics.BitmapFactory;

public class ShowResultsButton extends ButtonView{
	GameView view;

	public ShowResultsButton(GameView view) {
		super(BitmapFactory.decodeResource(view.getResources(), R.drawable.top));
		this.view = view;
	}

	public void press(GameManager gameManager) {
		gameManager.view().showTopTable();
	}

}
