package com.mass.candybuzzz.personages;

import android.graphics.Canvas;
import android.graphics.Point;
import com.mass.candybuzzz.HorizontalDirection;
import com.mass.candybuzzz.Matrix;
import com.mass.candybuzzz.VerticalDirection;

public class NullBehavior implements AbstractBehavior {

	public void update() {}

	public void draw(Canvas canvas) {}

	public String getType() {
		return this.getClass().toString();
	}

	public boolean isDied() {
		return false;
	}

	public void kill() {}

	public void saveAt(Matrix m, Point pos) {}

	public void move(Point pos, VerticalDirection direction) {}

	public void move(Point pos, HorizontalDirection direction) {}

	public void moveFinish(Point displacement, VerticalDirection direction) {}

	public void moveFinish(Point displacement, HorizontalDirection direction) {}

	public boolean isAvailable() {
		return true;
	}

}
