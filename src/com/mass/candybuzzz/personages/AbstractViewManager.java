package com.mass.candybuzzz.personages;

public interface AbstractViewManager {
	void setViewSize(int elWidth);
	void setViewCenter(int x, int y);
}
