package com.mass.candybuzzz.personages;

import com.mass.candybuzzz.GameView;

public class Buggy extends Mark {

	public Buggy(BehaviorDelegate delegate, GameView gameView) {
		super(delegate, gameView, gameView.gameField());
	}

}
