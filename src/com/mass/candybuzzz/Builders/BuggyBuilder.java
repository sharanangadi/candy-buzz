package com.mass.candybuzzz.Builders;

import java.util.ArrayList;
import java.util.List;

import android.graphics.Bitmap;
import com.mass.candybuzzz.GameView;
import com.mass.candybuzzz.personages.Buggy;
import com.mass.candybuzzz.personages.Mark;
import com.mass.candybuzzz.Builders.ImagesPool;
import com.mass.candybuzzz.views.ComposeSprite;
import com.mass.candybuzzz.views.LinearSprite;

public class BuggyBuilder extends EmptyMarkBuilder {

	protected Mark createPersonage(GameView view) {
		return new Buggy(null, view);
	}

	protected ComposeSprite createSprite(GameView view) {
		List<LinearSprite>sprites = new ArrayList<LinearSprite>();
		
		Bitmap img = ImagesPool.instance(view).getBug1();
		sprites.add(new LinearSprite(img, 4, 30, 1000));
		img = ImagesPool.instance(view).getBug2();
		sprites.add(new LinearSprite(img, 4, 30, 1000));
		
		ComposeSprite sprite = new ComposeSprite(sprites);
		return sprite;
	}
	
	protected boolean checkType(String type) {
		return type.contains("Bug");
	}

}
