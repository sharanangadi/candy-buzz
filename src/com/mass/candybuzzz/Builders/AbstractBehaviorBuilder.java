package com.mass.candybuzzz.Builders;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.mass.candybuzzz.GameView;
import com.mass.candybuzzz.personages.AbstractBehavior;
import com.mass.candybuzzz.personages.EmptyMark;
import com.mass.candybuzzz.personages.Mark;
import com.mass.candybuzzz.Builders.AbstractBehaviorBuilder;
import com.mass.candybuzzz.Builders.BirdBuilder;
import com.mass.candybuzzz.Builders.BuggyBuilder;
import com.mass.candybuzzz.Builders.CatBuilder;
import com.mass.candybuzzz.Builders.FrogBuilder;
import com.mass.candybuzzz.Builders.MouseBuilder;
import com.mass.candybuzzz.Builders.RabbitBuilder;


public class AbstractBehaviorBuilder {
	static List<AbstractBehaviorBuilder> builders = new ArrayList<AbstractBehaviorBuilder>();
	static boolean buildersNoCreated = true;
		
	public AbstractBehaviorBuilder() {
		if(buildersNoCreated) {
			buildersNoCreated  = false;
			builders.add(new CatBuilder());
			builders.add(new BuggyBuilder());
			builders.add(new MouseBuilder());
			builders.add(new BirdBuilder());
			builders.add(new FrogBuilder());
			builders.add(new RabbitBuilder());
			builders.add(new PigBuilder());
			builders.add(new BearBuilder());
		}
	}
	
	public AbstractBehavior create(GameView view) {
		Mark behavior = new EmptyMark(view);
		return behavior;
	}
	
	public AbstractBehavior createRandomMark(GameView view) {
		Random rnd = new Random();
		AbstractBehaviorBuilder builder = builders.get(rnd.nextInt(builders.size()));
		return builder.create(view);
	}
	
	protected boolean checkType(String type) {
		return false;
	}

	public AbstractBehavior createByName(GameView gameView, String name) {
		for(AbstractBehaviorBuilder builder: builders) {
			if(builder.checkType(name)) {
				return builder.create(gameView);
			}
		}
		return create(gameView);
	}
	
}