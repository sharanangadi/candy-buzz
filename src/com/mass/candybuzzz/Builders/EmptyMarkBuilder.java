package com.mass.candybuzzz.Builders;

import java.util.ArrayList;
import java.util.List;

import android.graphics.Bitmap;
import com.mass.candybuzzz.GameView;
import com.mass.candybuzzz.personages.AbstractBehavior;
import com.mass.candybuzzz.personages.AdditionViewBehavior;
import com.mass.candybuzzz.personages.Cloud;
import com.mass.candybuzzz.personages.KillerBehavior;
import com.mass.candybuzzz.personages.KillerWithEffectBehavior;
import com.mass.candybuzzz.personages.Mark;
import com.mass.candybuzzz.personages.SlowViewBehavior;
import com.mass.candybuzzz.personages.ViewsManager;
import com.mass.candybuzzz.Builders.ImagesPool;
import com.mass.candybuzzz.views.ComposeSprite;
import com.mass.candybuzzz.views.LinearSprite;

public class EmptyMarkBuilder extends AbstractBehaviorBuilder {

	public AbstractBehavior create(GameView view) {
		Mark behavior = createPersonage(view);
		ViewsManager viewManager = new SlowViewBehavior(behavior, createSprite(view), view.gameField()); 
		KillerBehavior killer = new KillerWithEffectBehavior(viewManager); 

		AdditionViewBehavior additionView = new AdditionViewBehavior(killer, createSprite(view), view.gameField());
		return additionView;
	}

	protected Mark createPersonage(GameView view) {
		return new Cloud(null, view);
	}

	protected ComposeSprite createSprite(GameView view) {
		List<LinearSprite>sprites = new ArrayList<LinearSprite>();
		
		Bitmap img = ImagesPool.instance(view).getEmpty();
		sprites.add(new LinearSprite(img, 1, 1130, 0));
		
		ComposeSprite sprite = new ComposeSprite(sprites);
		return sprite;
	}

	
	protected boolean checkType(String type) {
		return type.contains("Cloud");
	}
}
