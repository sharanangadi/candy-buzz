package com.mass.candybuzzz.Builders;

import java.util.ArrayList;
import java.util.List;

import android.graphics.Bitmap;
import com.mass.candybuzzz.GameView;
import com.mass.candybuzzz.personages.Mouse;
import com.mass.candybuzzz.personages.Mark;
import com.mass.candybuzzz.Builders.ImagesPool;
import com.mass.candybuzzz.views.ComposeSprite;
import com.mass.candybuzzz.views.LinearSprite;

public class MouseBuilder extends EmptyMarkBuilder {

	protected Mark createPersonage(GameView view) {
		return new Mouse(null, view);
	}

	protected ComposeSprite createSprite(GameView view) {
		List<LinearSprite>sprites = new ArrayList<LinearSprite>();
		
		Bitmap img = ImagesPool.instance(view).getMouse1();
		sprites.add(new LinearSprite(img, 4, 30, 0));
		
		ComposeSprite sprite = new ComposeSprite(sprites);
		return sprite;
	}
	
	protected boolean checkType(String type) {
		return type.contains("Mouse");
	}

}
