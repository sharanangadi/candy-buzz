package com.mass.candybuzzz;

import java.lang.ref.WeakReference;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Menu;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.google.ads.InterstitialAd;
import com.mass.candybuzzz.data.Config;
import com.mass.candybuzzz.data.DB;
import com.mass.candybuzzz.utils.AppRater;
import com.mass.candybuzzz.utils.Constants;

public class GameFieldActivity extends Activity implements AdListener{

    private Config conf;
    private DB db;
    private GameView view;
    FrameLayout linearLayout;
    AdView adView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        interstitialAd = new InterstitialAd(this, Constants.FULLAD_PUBLISHERS_CODE);
        interstitialAd.setAdListener(this);
        AdRequest adRequest = new AdRequest();
        adRequest.addTestDevice(AdRequest.TEST_EMULATOR);
        interstitialAd.loadAd(adRequest);

        conf = new Config(getPreferences(MODE_PRIVATE));
        db = new DB(this);

        adView = new AdView(this, AdSize.BANNER, Constants.AD_PUBLISHERS_CODE);
        adView.loadAd(new AdRequest());

        view = new GameView(this);

        // requesting to turn the title OFF
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        // making it full screen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        adView = new AdView(this, AdSize.SMART_BANNER, Constants.AD_PUBLISHERS_CODE);
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

        adView.setLayoutParams(lp);

        RelativeLayout layout = new RelativeLayout(this);
        layout.addView(view);
        layout.addView(adView);
        adView.loadAd(new AdRequest());
        setContentView(layout);
        TimerTask tt = new TimerTask() {

            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        adView.loadAd(new AdRequest());
                    }
                });

            }
        };

        Timer t = new Timer();
        t.scheduleAtFixedRate(tt, 0, 1500 * 60);
        AppRater.app_launched(new WeakReference<Activity>(this));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        view.showTopTable();
        return false;
    }

    protected void onStart() {
        super.onStart();
    }

    protected void onResume() {
        super.onResume();
    }

    protected void onPause() {
        super.onPause();
        saveToDB();
    }

    protected void onDestroy() {
        super.onDestroy();
        view.stop();
    }

    public void loadFromDB() {
        view.load(db, conf);
    }

    public void loadTopResults() {
        view.loadResults(db);
    }

    public void saveToDB() {
        view.save(db, conf);
    }
    
    
    /** The interstitial ad. */
    private InterstitialAd interstitialAd;

    /** Called when an ad is clicked and about to return to the application. */
    @Override
    public void onDismissScreen(Ad ad) {
    }

    /** Called when an ad was not received. */
    @Override
    public void onFailedToReceiveAd(Ad ad, AdRequest.ErrorCode error) {
    }

    /**
     * Called when an ad is clicked and going to start a new Activity that will
     * leave the application (e.g. breaking out to the Browser or Maps
     * application).
     */
    @Override
    public void onLeaveApplication(Ad ad) {
    }

    /**
     * Called when an Activity is created in front of the app (e.g. an
     * interstitial is shown, or an ad is clicked and launches a new Activity).
     */
    @Override
    public void onPresentScreen(Ad ad) {
    }

    /** Called when an ad is received. */
    @Override
    public void onReceiveAd(Ad ad) {
      if (ad == interstitialAd) {
          interstitialAd.show();
      }
    }
    
}
